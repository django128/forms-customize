from django.shortcuts import render
from .forms import StudentForm

# Create your views here.

def home(request):
    forms = StudentForm()
    context = {
        'forms': forms
    }
    return render(request, 'style_forms/index.html', context)