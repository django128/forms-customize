from django import forms
from .models import Student


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = "__all__"
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Votre matricul'
                }
            ),

            'roll': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Votre matricul'
                }
            ),
        }
